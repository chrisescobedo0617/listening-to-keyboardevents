document.addEventListener('keydown', logKey);

function logKey(e) {
    console.log(e.code)
    if (e.code === 'ArrowDown') {
        boxTop += 10
        document.getElementById("box").style.top = boxTop + "px";
    } else if (e.code === 'ArrowRight') {
        boxLeft += 10
        document.getElementById("box").style.left = boxLeft + "px";
    } else if (e.code === 'ArrowLeft') {
        boxLeft -= 10
        document.getElementById("box").style.left = boxLeft + "px";
    } else if (e.code === 'ArrowUp') {
        boxTop -= 10
        document.getElementById("box").style.top = boxTop + "px";
    }
}
let boxTop = 200;
let boxLeft = 200;